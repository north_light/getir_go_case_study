package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"go_case_study/config"
	"go_case_study/helper"
	"go_case_study/model/record"
	"go_case_study/repository"
	"go_case_study/response"
	"net/http"
)

type IRecordController interface {
	Find(ctx *gin.Context)
}

type RecordController struct {
	repository repository.IRecordRepository
}

func NewRecordController() IRecordController {
	mongoDb := config.GetMongoConnection(context.Background())
	return &RecordController{
		repository: repository.NewRecordRepository(*mongoDb.Collection(config.MongoRecordCollection, nil)),
	}
}

func (ctrl *RecordController) Find(ctx *gin.Context) {

	var filter record.Filter
	err := json.NewDecoder(ctx.Request.Body).Decode(&filter)
	if err != nil {
		response.ErrorResponse(ctx, err)
		return
	}

	startDate, err := helper.ParseTime(filter.StartDate)
	if err != nil {
		response.ErrorResponse(ctx, err)
		return
	}

	endDate, err := helper.ParseTime(filter.EndDate)
	if err != nil {
		response.ErrorResponse(ctx, err)
		return
	}

	if startDate.After(endDate) {
		response.ErrorMessageResponse(ctx, "Start Date must be less than End Date ")
		return
	}

	if filter.MinCount > filter.MaxCount {
		response.ErrorMessageResponse(ctx, "Max count must be greater than min count")
		return
	}

	recordResponse := ctrl.repository.FindRecord(filter)
	ctx.JSON(http.StatusOK, recordResponse)
}
