package controller

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"go_case_study/model/memory"
	"go_case_study/repository"
	"go_case_study/response"
	"net/http"
)

type IMemoryController interface {
	Get(ctx *gin.Context)
	Set(ctx *gin.Context)
}

type MemoryController struct {
	repository repository.IMemoryRepository
}

func NewMemoryController() IMemoryController {
	return &MemoryController{
		repository: repository.NewMemoryRepository(),
	}
}

func (ctrl MemoryController) Get(ctx *gin.Context) {
	key := ctx.Query("key")
	if key == "" {
		response.ErrorMessageResponse(ctx, "Key is null")
		return
	}

	result := memory.Memory{Key: key, Value: ctrl.repository.Get(key)}
	ctx.JSON(http.StatusOK, result)
}

func (ctrl MemoryController) Set(ctx *gin.Context) {

	var memory memory.Memory
	err := json.NewDecoder(ctx.Request.Body).Decode(&memory)
	if err != nil {
		response.ErrorResponse(ctx, err)
		return
	}

	if memory.Key == "" {
		response.ErrorMessageResponse(ctx, "Key is null")
		return
	}

	if memory.Value == "" {
		response.ErrorMessageResponse(ctx, "Value is null")
		return
	}

	ctrl.repository.Set(memory.Key, memory.Value)
}
