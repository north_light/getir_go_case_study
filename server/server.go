package server

import (
	"context"
	"fmt"
	"go_case_study/config"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type Server interface {
	Start()
}

func Start() {
	router := SetupRouter()

	server := &http.Server{
		Handler: router,
		Addr:    ":" + os.Getenv("PORT"),
	}

	go gracefulShutdown(server)

	fmt.Printf("server started on port: %s", config.Port)
	if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		fmt.Println("cannot start server.", err)
		panic("cannot start server")
	}
}

func gracefulShutdown(srv *http.Server) {
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	fmt.Println("Shutdown Server")

	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		fmt.Println("Server Shutdown Error: ", err)
	}

	fmt.Printf("Server exiting")
}
