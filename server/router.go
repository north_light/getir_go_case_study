package server

import (
	"github.com/gin-gonic/gin"
	"go_case_study/controller"
)

type Handler struct {
	memoryController controller.IMemoryController
	recordController controller.IRecordController
}

func SetupRouter() *gin.Engine {

	recordController := controller.NewRecordController()
	memoryController := controller.NewMemoryController()

	router := gin.New()

	router.POST("/record", recordController.Find)

	router.POST("/memory", memoryController.Set)
	router.GET("/memory", memoryController.Get)

	return router
}
