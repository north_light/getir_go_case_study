package helper

import "time"

func ParseTime(currentTime string) (time.Time, error) {
	newTime, err := time.Parse("2006-01-02", currentTime)
	if err != nil {
		return time.Time{}, err
	}
	return newTime, nil
}
