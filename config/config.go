package config

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var MongoDbURI = "mongodb+srv://challengeUser:WUMglwNBaydH8Yvu@challenge-xzwqd.mongodb.net/getir-case-study"
var MongoDatabase = "getir-case-study"
var MongoRecordCollection = "records"
var Port = "8080"

func GetMongoConnection(ctx context.Context) *mongo.Database {
	clientOptions := options.Client().ApplyURI(MongoDbURI)
	client, _ := mongo.Connect(ctx, clientOptions)

	return client.Database(MongoDatabase, nil)
}
