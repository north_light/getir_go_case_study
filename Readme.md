## Go Case Study

Project goal is get from mongodb and set to memory via endpoints.

App Deployed : https://go-case-study.herokuapp.com/

Default Port: 8080

HttpFramework: Gin

### REST APIs For Heroku
Fetch from MongoDB for record   
POST -> https://go-case-study.herokuapp.com/record

Fetch from Memory   
GET -> https://go-case-study.herokuapp.com/memory


Set to Memory   
POST -> https://go-case-study.herokuapp.com/memory


### REST APIs For Local
Fetch from MongoDB for record   
POST -> http://localhost:4545/record     

Fetch from Memory   
GET -> http://localhost:4545/memory  


Set to Memory   
POST -> http://localhost:4545/memory      

