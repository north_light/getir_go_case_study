package repository

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go_case_study/model/record"
	"gopkg.in/mgo.v2/bson"
	"time"
)

type IRecordRepository interface {
	FindRecord(filter record.Filter) *record.Response
}

type RecordRepository struct {
	collection mongo.Collection
}

func NewRecordRepository(collection mongo.Collection) IRecordRepository {
	return &RecordRepository{
		collection: collection,
	}
}

func (r *RecordRepository) FindRecord(filter record.Filter) *record.Response {

	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()

	var result record.Response
	result.Code = -1

	startDate, errDate := time.Parse("2006-01-02", filter.StartDate)
	endDate, errDate := time.Parse("2006-01-02", filter.EndDate)
	if errDate != nil {
		result.Msg = errDate.Error()
		return &result
	}

	pipeline := []bson.M{{"$project": bson.M{
		"key":        1,
		"createdAt":  1,
		"totalCount": bson.M{"$sum": "$counts"},
	}}, {"$match": bson.M{
		"$and": []bson.M{
			{"createdAt": bson.M{"$gte": startDate}},
			{"createdAt": bson.M{"$lt": endDate}},
			{"totalCount": bson.M{"$gte": filter.MinCount}},
			{"totalCount": bson.M{"$lt": filter.MaxCount}}},
	}},
	}

	cursor, err := r.collection.Aggregate(ctx, pipeline)
	if err != nil {
		result.Msg = err.Error()
		return &result
	}

	if err = cursor.All(ctx, &result.Records); err != nil {
		result.Msg = err.Error()
		return &result
	}

	if len(result.Records) != 0 {
		result.Code = 0
		result.Msg = "Success"
	}

	return &result
}
