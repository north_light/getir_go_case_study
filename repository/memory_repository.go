package repository

import (
	"go_case_study/model/memory"
)

type IMemoryRepository interface {
	Get(key string) string
	Set(key string, value string)
}

type MemoryRepository struct {
	memory memory.StoreMemory
}

func NewMemoryRepository() IMemoryRepository {
	return &MemoryRepository{memory: memory.StoreMemory{List: make(map[string]string)}}
}

func (r *MemoryRepository) Get(key string) string {
	r.memory.RLock()
	defer r.memory.RUnlock()
	return r.memory.List[key]
}

func (r *MemoryRepository) Set(key string, value string) {
	r.memory.Lock()
	defer r.memory.Unlock()
	r.memory.List[key] = value
}
