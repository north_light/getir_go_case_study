package test

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"go_case_study/model/record"
	"go_case_study/server"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var router *gin.Engine

func TestMain(m *testing.M) {
	router = server.SetupRouter()
	code := m.Run()
	os.Exit(code)
}

func TestItShouldOKIfPayloadIsValid(t *testing.T) {
	filter := record.Filter{
		StartDate: "2010-11-11",
		EndDate:   "2020-02-02",
		MinCount:  2700,
		MaxCount:  3000,
	}

	jsonBytes, _ := json.Marshal(filter)
	req, _ := http.NewRequest("POST", "/record", bytes.NewBuffer(jsonBytes))
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)
}

func TestItShouldNotOkIfNotValidPayload(t *testing.T) {
	filter := record.Filter{
		StartDate: "2010-11-11",
		EndDate:   "2020-02-02",
		MinCount:  5000,
		MaxCount:  1000,
	}

	jsonBytes, _ := json.Marshal(filter)
	req, _ := http.NewRequest("POST", "/record", bytes.NewBuffer(jsonBytes))
	response := executeRequest(req)

	checkResponseCode(t, http.StatusInternalServerError, response.Code)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	router.ServeHTTP(rr, req)
	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}
