package test

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"go_case_study/repository"
	"testing"
)

func TestGetData(t *testing.T) {
	memoryRepository := repository.NewMemoryRepository()
	memoryRepository.Set("test_key", "test_val")
	getValue := memoryRepository.Get("test_key")
	assert.Equal(t, "test_val", getValue, fmt.Sprintf("expected 'value', got: %s", getValue))
}
