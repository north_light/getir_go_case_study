package memory

type Memory struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}
