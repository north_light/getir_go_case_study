package memory

import "sync"

type StoreMemory struct {
	sync.RWMutex
	List map[string]string
}
