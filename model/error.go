package model

type RequestError struct {
	StatusCode int
	Err        error
}
