package record

type Filter struct {
	StartDate string `json:"startDate"`
	EndDate   string `json:"endDate"`
	MaxCount  int    `json:"maxCount"`
	MinCount  int    `json:"minCount"`
}
