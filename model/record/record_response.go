package record

type Response struct {
	Code    int      `json:"code"`
	Msg     string   `json:"val,omitempty"`
	Records []Record `json:"records,omitempty"`
}
