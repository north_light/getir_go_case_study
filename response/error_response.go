package response

import (
	"errors"
	"github.com/gin-gonic/gin"
	"go_case_study/model"
	"net/http"
)

func ErrorResponse(ctx *gin.Context, err error) {
	requestError := model.RequestError{StatusCode: http.StatusInternalServerError, Err: err}
	ctx.JSON(requestError.StatusCode, requestError)
}

func ErrorMessageResponse(ctx *gin.Context, err string) {
	ErrorResponse(ctx, errors.New(err))
}
